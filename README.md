# Branch pipelines

Ensure pipeline runs for tags and branches, except for merge request events.

This project contains a single default component that allows to run pipelines:
- for changes to the `main` branch.
- when a tag is created.

## Usage

```yaml
include:
  - component: gitlab.com/gitlab-test-ci-catalog/catalog/branch-pipelines@1.0
```